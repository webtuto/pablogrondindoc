# Website

Ce site a été construit avec [Docusaurus 2](https://docusaurus.io/), a modern static website generator.

## Publier avec Knative service nos container
```bash
# Vue d'ensemble sur les namespace
kubectl get ns

#Lister nos services knativs
kubectl get ksvc

#Lister nos révision

kubectl get revisions

```
## Todo 24062022
* Corriger Progrmmtion > python
* Revoir ordre apparition des tutos OK
* Corriger bug version OK 
* Tuto backup
    * Voir les 3 outils `rclone`, `minIO`et `openIO` et `Veeam`, `Cohesity` 
    * Bien différencier de Proxmox
* Tuto Proxmox
* Tuto DockerFile
* 
# Render 
* [Version pour package docusaurus](https://www.npmjs.com/package/@docusaurus/core)
* [Le lien pour docusaurus](https://render.com/docs/deploy-docusaurus) 
* [Le ficheir render](https://render.com/docs/blueprint-spec#databases)
## Outil de personnalistion de la doc
1. Conversion des iamges
* [Conversion PNG en SVG](https://convertio.co/fr/download/f906fa520a21f32f261b612bbe8f7a52cf74da/)
* [Conversion PNG en ICO](https://www.online-convert.com/result#j=a6d8bc65-f926-4d85-9b4e-aa71e073e57f)

2. [Site utilisé pour les icons](https://www.flaticon.com/)

3. [Les cadre colorés pour markdown](https://docusaurus.io/docs/markdown-features/admonitions)

* [Exemple de bout en bout](https://medium.com/@mutebg/using-gitlab-to-build-test-and-deploy-modern-front-end-applications-bc940501a1f6)
## Pour faire des tests poste déploiement

* [LightHouse](https://github.com/GoogleChrome/lighthouse)
* [Speedio](https://docs.gitlab.com/ee/ci/testing/browser_performance_testing.html)
## Release une nouvelle version 
```bash
npm run docusaurus docs:version 1.1
```
## Installation

```
$ yarn
```

## Local Development

```
$ yarn start
$ npm start

```

This command starts a local development server and opens up a browser window. Most changes are reflected live without having to restart the server.

## Build

```bash
$ yarn build
$ npm run build
```

This command generates static content into the `build` directory and can be served using any static contents hosting service.

[To specifie a port and host](https://docusaurus.io/fr/docs/deployment#environment-settings)

```bash
yarn run serve -- --build --port 80 --host 0.0.0.0

```
## Déploiement

Using SSH:

```
$ USE_SSH=true yarn deploy
```

Not using SSH:

```
$ GIT_USER=<Your GitHub username> yarn deploy
```

If you are using GitHub pages for hosting, this command is a convenient way to build the website and push to the `gh-pages` branch.
### Build en local
```
docker build -t pablogrondindoc:0.2 .
```
### Lancer en local
```bash
docker run -p 80:8080 pablogrondindoc:0.2
```
### Gitlab

1. Pousser sur Artifact repository. **Prérequis** : Déploiement container fonctionnel sur port 127.0.0.1:80
  1. S'authentifier :`gcloud auth configure-docker europe-west9-docker.pkg.dev`
  2. Ajouter un TAG à notre image locale: `docker tag <DOCKER_LOCAL:VERSION> <Localisation>-docker.pkg.dev</PROJECT_ID>/<REPO>/<TAG_CONTENEUR>` 
     *  `docker tag pablogrondindoc:0.2 europe-west9-docker.pkg.dev/iaacgitlab/docusaurus/pablogrondindoc:0.1.0`
  3. Pousser `docker push <Localisation>-docker.pkg.dev</PROJECT_ID>/<REPO>/<TAG_CONTENEUR>`
    * `docker push europe-west9-docker.pkg.dev/iaacgitlab/docusaurus/pablogrondindoc:0.1.0`
* [Localisation des artifacts registry](https://cloud.google.com/artifact-registry/docs/repositories/repo-locations?hl=fr)

```bash
gcloud auth configure-docker europe-west9-docker.pkg.dev

docker tag pablogrondindoc:0.2 europe-west9-docker.pkg.dev/iaacgitlab/docusaurus/pablogrondindoc:0.1.0 

docker push europe-west9-docker.pkg.dev/iaacgitlab/docusaurus/pablogrondindoc:0.1.0

```
2. Poursser sur le container repository
```bash
docker tag pablogrondindoc:0.2 eu.gcr.io/iaacgitlab/pablogrondindoc:0.1.0   
docker push eu.gcr.io/iaacgitlab/pablogrondindoc:0.1.0
``` 
