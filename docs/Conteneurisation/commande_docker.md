---
sidebar_position: 2
---

# Commandes docker
* Image par défaut : OS alpine
* Pas de systemd, init car ce n'est pas un OS
* Pour trouver des conteneur, [docker hub](https://hub.docker.com/). On peut aussi avoir un registre privé Gitlab, 

```bash
# Afficher les conteneurs lancé
docker ps 

# Afficher tous les conteneurs avec un statut arrete
docker ps -a 

# Lancer un conteneur
docker run {PARAMETRES_1} {PARAMETRES_2} 

# Préciser la version voulue
docker run alpine:latest #: pour avoir numero de version

# Pour démarrer un conteneur déjà crée
docker start {NOMCONTENUR}
docker stop {NOMCONTENEUR}

# Supprimer et néttoyer
docker rm -f {NOM}
docker system prune # Clean le réseau, cache, images
 
```

* Un `docker run ` effectue un `docker pull` si l'image est inconnu

Option courantes : 

* `-d detach`
* `-i interaction`
* `--name` pour préciser le nom, utilise pour désigner le conteneur
* `--hostname` nom de la machine 
* `-t` tty pour se connecter

```bash
# Pour pouvoir accéder au CLI du conteneur
docker run -di --name pabloAlpine alpine:latest

# Pour se connecter au conteneur 
docker exec --ti pabloAlpine sh 
```
Une fois connecté au CLI :  

* `ps`
* `ls`
* `exit` pour sortir



## Gestion des ports, lancer un serveur web/Réseau

* `-p` Exposition de port Un port du conteneur vers un port de notre machine local 
* `-p 8080:80` *PATH_MACHINE_LOCAL:PATH_CONTENEUR*

```bash
docker run -tid --name web -p 8080:80 nginx:latest
```
## Volumes
Par défaut, les données ne sont pas persistentes.

Il y a deux 2 types de volumes pour la persistance des données
   * Les *volume* avec les options `-v` ou `volume` 
      * `-v PATH_VM:PATH_DOCKER`
      * `docker volume` Gestion des volumes avec docker. On 
         * `create`
         * `inspect` # Voir ou il a crée le volume dans la VM
         * `ls`
         * `prune`
         * `rm` On doit supprimer le conteneur avant
   * Option *mount* `--mount source={NOMVOLUMES},target={DOCKER_PATH}`

L'actualisation des données se fait automatiquement.  

```bash
# Option -v 
docker run -tid --name web -p 8080:80 -v `/home/pablo/web/:/usr/share/nginx/html/ nginx:latest

# Gestion des volumes 
docker volume 
# Creer le volume volume31102021
docker volume create volume31102021
docker volume ls
docker volume inspect volume31102021
docker run -tid --name web -p 8080:80 --mount source=volume31102021,target=/usr/share/nginx/html nginx:latest

```
* ***Notes*** Sur Windows/OS X le mount KO. Docker va créer une [virtualBox machine](https://stackoverflow.com/questions/36830064/docker-volume-mount-doesnt-exist) qu'il faut lancer.

```bash
# Pour voir le volume
docker-machine ssh default 

```




