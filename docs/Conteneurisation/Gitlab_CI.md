---
sidebar_position: 3
---


# Build

## Gitlab 

### Contruire un conteneur et enregistrement dans le registry
* Utiliser la *Gitlab repository* afin d'héberger ses conteneurs 
```bash
build:
  stage: build
  image: docker
  services:
    - docker:18-dind
  script: 
    
    - docker info
    - echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER $CI_REGISTRY --password-stdin
    - docker build -t $CI_REGISTRY_IMAGE:latest .
    - docker push $CI_REGISTRY_IMAGE:latest
  only:
    - master
```

### Kebernete et Civo 
* Connection au cluster kubernete sur Civo 
* Deux variables d'environnement dans *Setting > CI/CD > Variable* 
   * `CIVO_API`, API key de civo 
   * `KUBECONFIG` : le fichier kubeconfig
```bash
build:
    image: dtzar/helm-kubectl
    stage: build
    script:
        - wget "https://github.com/civo/cli/releases/download/v0.7.6/civo-0.7.6-linux-amd64.tar.gz"
        - "tar -xvf civo-0.7.6-linux-amd64.tar.gz"
        - "chmod +x civo"
        - "mv ./civo /usr/local/bin/"

        - "civo apikey add civoKey $CIVO_API"
        - "civo apikey current civoKey"
        - "civo apikey list" 

        - "kubectl config set-context CIVO --cluster='Terraform-civo-Helm' --kubeconfig=$KUBECONFIG" #Remplacer par votre nom de cluster
        - "kubectl get pods -n knative-serving"

        - "kubectl get svc"
        - "kubectl get ns"


```
### Contruire un package Helm et enregistrement dans le registry 
* Aucune variable d'environnement à sourcer
```bash
build:
    image: dtzar/helm-kubectl
    stage: build
    before_script:
      - 'helm repo add --username gitlab-ci-token --password ${CI_JOB_TOKEN} ${CI_PROJECT_NAME} ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/stable'

    script:
        - "helm package exemple_chart"
        
        - 'helm plugin install https://github.com/chartmuseum/helm-push.git'
        - "helm cm-push /builds/webtuto/pablogrondindoc/exemple_chart-0.1.0.tgz ${CI_PROJECT_NAME}"
```
## Déploiement 
* [Heroku](https://devcenter.heroku.com/categories/heroku-architecture)
   * [Un exemple](https://medium.com/swlh/how-do-i-deploy-my-code-to-heroku-using-gitlab-ci-cd-6a232b6be2e4)