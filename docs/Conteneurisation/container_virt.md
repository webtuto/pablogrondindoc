---
sidebar_position: 1
---


# Conteneurisation et virtualisation


![Image-Conteneur_VM](/img/Image-Conteneur_VM.png)

Les conteneurs isolent un ou plusieurs processus et virtualisent le système d'exploitation. Les conteneurs font abstraction du matériel CPU, GPU, Mémoire vive, Disque durs, système de fichier. [Cela permet](https://devopssec.fr/article/differences-virtualisation-et-conteneurisation) :

* Un gain d'espace considérable
   * Rarement au dessus de plusieurs MB
* Un gain de puissance nécessaire au fonctionnement d'un processus 
* Des améliorations de sécurité
   * Cloisement des fonctionnalités
* Des améliorations de fiabilités
   * Haute disponibilité 


* Des gains lors des **déploiements** 
   * Définition des conteneurs avec un fichier appelé `Dockerfile` docker
      * *Versionning* des environnements voulus
      * Déploiement cloud via le `Dockerfile`
* Conception, mise en oeuvre et maintenance facilié d'envionnements (développement, prod, simulation par exemple): dockerisation et l'Infrastructure As A Code. 

* Automatisation :  des tests et déploiement avec les pipelines `CI/CD`  
 
* Mise à l'échelle 
   

## Les moteurs de conteneurs et orchestrateur
* Pour n'en citer que quelque un  
    * `docker`
    * `LXC`  

2. C'est l'orchestrateur qui gère nos conteneurs. [Selon Redhat](https://www.redhat.com/fr/topics/containers/what-is-container-orchestration), l'orchestration des conteneurs permet d'automatiser le déploiement, la gestion, la mise à l'échelle et la mise en réseau des conteneurs.  Plusieurs solutions pour nous aider à gérer des déployer des conteneurs. Pour en citer quelque un :  
   * `kubernetes` : puissant mais complexe. Il devient le leeader du marché. 
   * `docker swarm` : simple mais limité

3. Les outils kubernetes
* [Helm](https://helm.sh/) . Package manager pour Kubernetes
* [Kustomisation](https://kustomize.io/). Gestion de la configuration

## Plateform en tant que service, *Paas*

Cette solution cloud computing, parfois installable sur site met à disposition un environnement d'éxécution. L'utilisateur de la *Paas* peut installer, configurer et tester les environnements selon leurs besoins. Ces plateformes proposent généralement :
* Des outils de versionnings comme `git`
* Des fonctionnalités *d'infrastructure as a service* 
* Un moteur de conteneur et un orchestrateur
* Des pipelines `CI/CD`intégrées au *Paas* afin de tester et déployer le code dans les environnements voulus

Pour citer quelques solutions *Paas* :
   * [GitLab](https://about.gitlab.com/fr-fr/), une plateforme DevOps. [Propose dans le cloud ou sur site](https://about.gitlab.com/why-gitlab/) une gestion du code source `git` indépendamment du langage, intégration et livraison continu `CI/CD`, un outil de gestion de projet, des fonctionnalités de DevSecOps (analyse des commits), gestion des bugs, gestion des packages avec les packages repositoy et container registry. 
 
   * [OpenShift](https://www.redhat.com/fr/technologies/cloud-computing/openshift) de `Redhat`qui utilise Docker et kubernetes comme technologie. Déploiement sur le cloud, sur site ou hybride.

   * [Heroku](https://www.heroku.com/home) avec des containers nommés dynos, supporte les langages suivants :
      * Prend en charge : `Node.js`, `Java`, `Ruby`, `Go`, `Python`, `PHP`, `Scala`, `Clojure` 
   * [Platform.sh](https://platform.sh/regions/france/). Solution cloud afin de déployer des frameworks comme `Wordpress`, `Drupal`, `Symphony`, `Magento` 
   * [Google App Engine](https://cloud.google.com/appengine).
      * Prend en charge `Node.js`, `Java`, `Ruby`, `C#`, `Go`, `Python` ou `PHP`.
   * [Microsoft Azure App Service](https://azure.microsoft.com/en-us/services/app-service/#overview)
      * Prend en charge : `Node.js`, `Java`, `Python`, `PHP` et environnements `.NET`
   * [Render](https://render.com/). 
      * Propose le déploiement de *Dockerfiles*, langage courants et framework Django, Rail&Epress, GraphQL&REST
      * L'hébergement gratuite de Web Service, Cron job, Database PostGreSQL et redis entièrement managé 
      * Plusieurs envrionnement
   * [Koyeb](https://www.koyeb.com/) 
      * Propose le déploiement de *Dockerfiles*, langage courants
      


### Notes - Trouveshooting
* Pour publier sur les `Gitlb Page`, le nom de l'étape `deploy` **doit** etre `pages` afin que l'artifact s'appel `pages:archive`.
* Exemple, de déploiement depuis une image de conteneur spécifique
```YAML
pages:
  stage: deploy
  image: $CI_REGISTRY_IMAGE
  script:
  - echo $VAR1
  - ls -a
  - mv /build public

  artifacts:
    paths:
    - public
```
*
