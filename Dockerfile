FROM node:18-alpine

COPY . .
#COPY package*.json ./
RUN yarn install
RUN yarn build 
CMD ["yarn","start","--port","80","--host","0.0.0.0"]
